


/*
 * Adapted from 
 * https://github.com/sparkfun/SparkFun_MPU-9250_Breakout_Arduino_Library/blob/master/examples/MPU9250BasicAHRS_SPI/MPU9250BasicAHRS_SPI.ino
 * 
 */
#include "quaternionFilters.h"
#include "MPU9250.h"
#include <SPI.h>


// Pin definitions
#define cs0 25
#define cs1 28
#define SPIport SPI
#define SPIspeed 1000000 //1MHz
#define serialRate 115200
MPU9250 imu1(cs0,SPI,SPI_DATA_RATE);
MPU9250 imu2(cs1,SPI,SPI_DATA_RATE);

void setup(){
  Serial.begin(serialRate);
  int stat1;
  int stat2;
  pinMode(cs0,OUTPUT);
  pinMode(cs1,OUTPUT);
  //Initialization of cs pins to HIGH so we can set one to low later
  digitalWrite(cs0,HIGH);
  digitalWrite(cs1,HIGH);


  //Well try enabling and setting up the first imu

  digitalWrite(cs0,LOW);
  SPIport.begin();
  imu1.kickHardware();
  byte c = imu1.readByte(9001,WHO_AM_I_MPU9250);
  Serial.print(F("MPU9250 I am 0x"));
  Serial.print(c,HEX);
  if(c==0x71){
    imu1.MPU9250SelfTest(imu1.selfTest);
    Serial.print(F("x-axis self test: acceleration trim within : "));
    Serial.print(imu1.selfTest[0],1); Serial.println("% of factory value");
    Serial.print(F("y-axis self test: acceleration trim within : "));
    Serial.print(imu1.selfTest[1],1); Serial.println("% of factory value");
    Serial.print(F("z-axis self test: acceleration trim within : "));
    Serial.print(imu1.selfTest[2],1); Serial.println("% of factory value");
    Serial.print(F("x-axis self test: gyration trim within : "));
    Serial.print(imu1.selfTest[3],1); Serial.println("% of factory value");
    Serial.print(F("y-axis self test: gyration trim within : "));
    Serial.print(imu1.selfTest[4],1); Serial.println("% of factory value");
    Serial.print(F("z-axis self test: gyration trim within : "));
    Serial.print(imu1.selfTest[5],1); Serial.println("% of factory value");

    // Calibrate gyro and accelerometers, load biases in bias registers
    imu1.calibrateMPU9250(imu1.gyroBias, imu1.accelBias);


    imu1.initMPU9250();
    // Initialize device for active mode read of acclerometer, gyroscope, and
    // temperature
    Serial.println("MPU9250 initialized for active data mode....");

    // Read the WHO_AM_I register of the magnetometer, this is a good test of
    // communication
    byte d = imu1.readByte(AK8963_ADDRESS, WHO_AM_I_AK8963);
    Serial.print("AK8963 ");
    Serial.print("I AM 0x");
    Serial.print(d, HEX);
    Serial.print(" I should be 0x");
    Serial.println(0x48, HEX);
    imu1.initAK8963(imu1.factoryMagCalibration);
    Serial.println("AK8963 initialized for active data mode....");
    imu1.getAres();
    imu1.getGres();
    imu1.getMres();
  }
  else
  {
    Serial.print("Could not connect to MPU9250: 0x");
    Serial.println(c, HEX);

    // Communication failed, stop here
    Serial.println(F("Communication failed, abort!"));
    Serial.flush();
    abort();
  }
  stat1 = imu1.begin();
  digitalWrite(cs0,HIGH);
  
  
  while(!Serial){
    
  }
  
}

void loop(){
  
}
char* readData(){
   
}
